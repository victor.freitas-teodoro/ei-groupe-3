import cv2
import numpy as np
from . import detect

def segments(img, line_array, color, thickness):
    "draws segments using only the first four numbers of each line in the lines list"
    current_img = img
    for line in line_array:
        x1,y1,x2,y2,a,b,c,n = line.astype(int)
        current_img = cv2.line(current_img, (x1,y1), (x2,y2), color, thickness)
    
    return current_img


def lines(img, line_array, color, thickness):
    "draws segments using only the line param of each line in the lines list"
    current_img = img
    height, width = img.shape[0], img.shape[1]
    for line in line_array:
        x1,y1,x2,y2,a,b,c,n = line.astype(np.float32)
        if b == 0:
            point_0 = (-c/a, 0)
            point_1 = (-c/a, height)
        elif a == 0:
            point_0 = (0, -c/b)
            point_1 = (width, -c/b)
        else:
            if a > 0:
                point_0 = (0, -c/b)
                point_1 = (-c/a, 0)
            else:
                point_0 = (width, -(c + a*width)/b)
                point_1 = (-c/a, 0)
        point_0 = (int(point_0[0]), int(point_0[1]))
        point_1 = (int(point_1[0]), int(point_1[1]))
        current_img = cv2.line(current_img, point_0, point_1, color, thickness)
    return current_img

def paint(img, points, radius, color, thickness):
    "draws around a set of points"
    n,m,d = img.shape
    for point in points:
        point = (int(point[0]), int(point[1]))
        img = cv2.circle(img, point, radius, color, thickness)
        
    return img

def lsd(img, color1=(255,0,0), color2=(0,255,0), thickness=2, radius=5, cutoff=20, draw_intersections=False):
    """draws the line segments on an image"""
    line_array = detect.lines(img)
    if len(line_array) != 0:
        line_array = detect.longer_than(line_array,cutoff)
        img = segments(img, line_array, color1, thickness)
        if draw_intersections:
            intersections = detect.intersect(line_array)
            img = paint(img, intersections, radius, color2, thickness)
    return img, line_array
    

def vanishing_point(img, vp, supporting_lines=None, color1=(255,0,0), color2=(0,255,0), thickness=2, 
        radius=5, confidence=.10):
    """draws the vanishing point on an image given it's line segments"""

    if vp[0] < float("inf"):
        img = cv2.circle(img, vp, radius, color1, thickness)
    if supporting_lines is None:
        return img
    img = lines(img, supporting_lines, color2, thickness)
    return img
        
def function(img, Y, hmin, hmax, ymin, ymax, color, thickness):
    """
    draws a function on top of an image
    """

    try:
        Y = (Y - ymin)/(ymax - ymin)*(hmax - hmin) + hmin # scaling values
        X = np.arange(img.shape[1])
        xi, yi = X[0], Y[0]
        xi, yi = int(xi), int(yi)
        for xf,yf in zip(X[1:],Y[1:]):
            xf, yf = int(xf), int(yf)
            img = cv2.line(img, (xi, yi), (xf,yf), color, thickness)
            xi, yi = xf,yf
        return img
    except ValueError:
        return img

def depth(img1, img2, direction, n=None, color=(0,255,0),
          window_size=20, range_shifts=10, filter_window=19):
    """
    draws the local shift between the images on img1 
    """

    if n is None:
        n = img1.shape[0]//2
    
    line1 = img1[n,:]
    line2 = img2[n,:]
    func = detect.local_shift(line1,line2,
    window_size, range_shifts, direction, filter_window)

    if len(img1.shape) < 3:
        img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2RGB)
    img = function(img1, np.array([0]*img1.shape[1]), n-1, n+1, -1, 1, (255, 255, 255), 2)
    img = function(img, func, n-50, n, 0, range_shifts, color, 2)
    return img