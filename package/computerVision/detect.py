import cv2
import numpy as np
import sys

def longer_than(lines, min_lenght):
    """removes the lines shorter than min_lenght"""
    return lines[lines[:,-1] >= min_lenght]

def canonical_line_eqs(segments):
    """computes the canonical lien equations of an iterable of line segments (x1,y1,x2,y2)"""
    lines = np.hstack((segments, np.zeros((len(segments),4))))
    lines[:,4] = lines[:,3] - lines[:,1]
    lines[:,5] = lines[:,0] - lines[:,2]
    lines[:,6] = -lines[:,4]*lines[:,0] - lines[:,5]*lines[:,1]

    # putting them in canonical form
    sign = np.sign(lines[:,5])
    sign[sign == 0] = 1
    alpha = sign*np.sqrt(lines[:,4]**2 + lines[:,5]**2)
    lines[:,4] = lines[:,4]/alpha
    lines[:,5] = lines[:,5]/alpha
    lines[:,6] = lines[:,6]/alpha

    # segment lenght 
    lines[:,7] = np.sqrt((lines[:,0] - lines[:,2])**2 + (lines[:,1]-lines[:,3])**2)
    return lines


def intersect(lines):
    """returns an array of intersections among the given line array"""
    intersections = []
    for i, si in enumerate(lines):
        for sj in lines[i+1:]:
            cross_product = np.cross(si[4:6], sj[4:6]) # [a1,b1] ^ [a2, b2]
            if cross_product != 0:
                coeff = 1.0 / cross_product

                intersections.append([coeff * np.cross(si[5:7]   , sj[5:7]), # [b1, c1] ^ [b2, c2]
                                      coeff * np.cross(sj[[4, 6]], si[[4, 6]])]) # -[a1, c1] ^ [a2, c2]
    return np.array(intersections)

def lines(gray_img):
    """returns a list of line segments on a gray scale image"""
    lsd_detector = cv2.ximgproc.createFastLineDetector()
    lsd_segments = lsd_detector.detect(gray_img)
    if lsd_segments is not None:
        return canonical_line_eqs(lsd_segments.reshape(-1,4))
    return []

def sci(values, confidence) :
    """
    values : an array of scalars (e.g. [1.2, 100.6, 23.78, ....])
    confidence : in [0,1], (e.g 0.5 for 50%)
    """
    nb        = values.shape[0]
    values    = np.sort(values)
    size      = (int)(nb*confidence+.5)
    nb_iter   = nb - size + 1
    sci       = None
    sci_width = sys.float_info.max
    inf       = 0
    sup       = size
    for i in range(nb_iter) :
        sciw = values[sup-1] - values[inf]
        if sciw < sci_width :
            sci       = values[inf:sup]
            sci_width = sciw
        inf += 1
        sup += 1
    # The result is the array (ordered) of the values inside the sci.
    return sci

def best_lines(lines, shortest=20, lower=0.2, upper=0.8):
    "removes vertical and horizontal lines"
    if type(lines) == list:
        return np.array(lines).reshape((-1,2))
    lines = longer_than(lines, shortest)
    lines = lines[lines[:,5] < upper]
    lines = lines[lines[:,5] > lower]
    return lines

def vanishing_point(line_array, confidence = 0.8):
    """
    returns the vanishing point coordniates given a set of lines
    and a confidence value. It'll return "inf" if there's no vanishing point
    """
    intersections = intersect(line_array)
    # minimum of three intersections
    if len(intersections) > 3:
        x_sci_mean = sci(intersections[:,0],confidence).mean().astype(np.float32)
        y_sci_mean = sci(intersections[:,1],confidence).mean().astype(np.float32)
        return x_sci_mean, y_sci_mean
    return float("inf"), float("inf")

def vp_lines(lines, vp, max_dist=50):
    """
    returns the left and right mean of the lines near
    enough to the vanishing point
    """
    if len(lines) == 0:
        return None
    params = lines[:,4:7]
    

    dists = (params @ np.hstack((vp,1)).reshape(3,1)).reshape(-1)
    valid_lines = lines[np.abs(dists) <= max_dist]
    if len(valid_lines) > 0:
        rlines = valid_lines[valid_lines[:,4] < 0]
        llines = valid_lines[valid_lines[:,4] > 0] 
        mrline = np.mean(rlines, axis=0)
        mlline = np.mean(llines, axis=0)

        out = []
        if not np.any(np.isnan(mrline)):
            out.append(mrline)
        if not np.any(np.isnan(mlline)):
            out.append(mlline)
        return np.array(out)
    return None

def wall_dist(lines, vp):
    """returns a number that's closer to 0 the closer the 
    drone should be to the left wall and closer to inf the
    closer it is to the right wall.
    
    in runaway cases it returns inf"""
    lines = vp_lines(lines, vp)
    if lines is None:
        return float("inf")
    if len(lines) == 1:
        return float("inf")
    br, bl = lines[:,5]
    return br - bl

def _shift(x, line1, line2, window_size=20, range_shifts=20, direction=-1):
    """
    computes the shift at x between two signals. a direction of -1 indicates that
    the drone is moving leftwards and a direction of 1 that it's moving forwards.
    line2 is the one that will be shifted.
    """
    # stack of the range of shifts
    p = window_size
    st = direction*range_shifts
    stack = np.vstack([line2[x-p+s:x+p+s+1] for s in range(0,st,direction)])
    reduce_sum = np.sum((stack - line1[x-p:x+p+1])**2, axis=1)
    return np.argmin(reduce_sum)

def local_shift(line1, line2, window_size=20, range_shifts=10, direction=-1, filter_window=19):
    """computes the shift between two signals. a direction of -1 indicates that
    the drone is moving leftwards and a direction of 1 that it's moving forwards.
    line2 is the one that will be shifted."""

    # pixels that would lead to an out of bounds error are ignorated
    ignore_left = window_size
    ignore_right = window_size + 1
    if direction == -1:
        ignore_left += range_shifts
    else:
        ignore_right += range_shifts
    indexes = np.arange(ignore_left, len(line1)-ignore_right)

    line1 = np.pad(medfilt(line1, filter_window),(filter_window//2, filter_window//2), "edge")
    line2 = np.pad(medfilt(line2, filter_window),(filter_window//2, filter_window//2), "edge")

    res = np.array([_shift(i, line1, line2, window_size=window_size, range_shifts=range_shifts, direction=direction) for i in indexes])
    res = np.pad(res, (ignore_left, ignore_right), "constant", constant_values=(0,0))
    return res

def _stride(a, L, S ):
    """
    from https://stackoverflow.com/questions/40084931/taking-subarrays-from-numpy-array-with-given-stride-stepsize/40085052#40085052
    """ 
    nrows = ((a.size-L)//S)+1
    n = a.strides[0]
    return np.lib.stride_tricks.as_strided(a, shape=(nrows,L), strides=(S*n,n))

def medfilt(array, size):
    return np.median(_stride(array, size,1),axis=1)
