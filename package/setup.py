import setuptools

setuptools.setup(
    name="cvis",
    version="1.0",
    author="Pedro Rosa",
    author_email="pedro.sacramento-xavier-barreto-rosa@student-cs.fr",
    description='Image Processing package for the ST5 EI',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
