#!/usr/bin/env python

from std_msgs.msg import Empty
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class Land(db.Behavior):
    def __init__(self):
        super().__init__("Land")
        
        self.pub_land=rospy.Publisher("land",Empty,queue_size=1)


    def on_status_on(self):
        #print("toto")
        self.pub_land.publish(Empty())
        rospy.sleep(2)
        self.set_status()

if __name__ == '__main__':

    rospy.init_node('Land')

    takeoff=Land()

    rospy.spin()
