#!/usr/bin/env python

from std_msgs.msg import Float32
from std_msgs.msg import Empty 
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Quaternion
import numpy as np
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point
from time import time 



class Slide(db.Behavior):
    def __init__(self):
        super().__init__("Slide")

        self.position_odo = Point()
        self.quaternion_odo = Quaternion()

        #Suscriber
        self.odom_sub = rospy.Subscriber('/odom',Odometry, self.on_odom, queue_size=1)
        
        #Publisher
        self.pub_rotate = rospy.Publisher("/angz",Float32,queue_size=1)
        self.pub_linx = rospy.Publisher("/linx",Float32,queue_size=1)
        self.pub_liny = rospy.Publisher("/liny",Float32,queue_size=1)    
        self.pub_hover = rospy.Publisher("/hover",Empty,queue_size=1)

        #Rviz publication 
        self.pub_axis = rospy.Publisher("slide_axis", Marker, queue_size=1)


    def on_odom(slef,msg):
        with self.data_mutex :
            self.position_odo = msg.pose.pose.position
            self.quaternion_odo = msg.pose.pose.quaternion



    def on_status_on(self):
        with self.data_mutex :
            #Definition de la droite
            vect_v = np.array([self.quaternion_odo.w**2 + self.quaternion_odo.x**2 -self.quaternion_odo.y**2 -self.quaternion_odo.z**2, 2 * self.quaternion_odo.w * self.quaternion_odo.z +  2 * self.quaternion_odo.x * self.quaternion_odo.y, 0])
            op0 = np.array([self.position_odo.x,self.position_odo.y,self.position_odo.z])

        # Rviz
        marker = Marker()
        marker.header.frame_id = "odom" 
        marker.type = marker.LINE_STRIP
        marker.lifetime = rospy.Duration(secs=1000)

        # marker scale
        marker.scale.x = 0.03
        marker.scale.y = 0.03
        marker.scale.z = 0.03

        # marker color
        marker.color.a = 1.0
        marker.color.r = 1.0
        marker.color.g = 1.0
        marker.color.b = 0.0

        # marker orientaiton
        marker.pose.orientation.x = 0.0
        marker.pose.orientation.y = 0.0
        marker.pose.orientation.z = 0.0
        marker.pose.orientation.w = 1.0

        # marker position
        marker.pose.position.x = 0.0
        marker.pose.position.y = 0.0
        marker.pose.position.z = 0.0

        # marker line points
        marker.points = []
        point = Point()
        point.x = self.position_odo.x - 10 * vect_v[0]
        point.y = self.position_odo.y - 10 * vect_v[1]
        point.z = self.position_odo.z 
        marker.points.append(point)


        point = Point()
        point.x = self.position_odo.x + 10 * vect_v[0]
        point.y = self.position_odo.y + 10 * vect_v[1]
        point.z = self.position_odo.z 

        marker.points.append(point)

        self.pub_axis.publish(marker)

        # Commande Slide 
        msg_linx = Float32()
        msg_liny = Float32()
        msg_rotate = Float32()

        msg_linx.data = 0.5
        msg_liny.data = 0.5
        msg_rotate.data = 3
            
        # Publication 
        self.pub_linx.publish(msg_linx)
        self.pub_rotate.publish(msg_rotate)

        rospy.sleep(1)

        # Remise en place
        self.pub_hover.publish(Empty())

        temps = time()
        fin = temps + 4

        while temps < fin:

            with self.data_mutex :
                #Definition de la droite
                forward = np.array([self.quaternion_odo.w**2 + self.quaternion_odo.x**2 -self.quaternion_odo.y**2 -self.quaternion_odo.z**2, 2 * self.quaternion_odo.w * self.quaternion_odo.z +  2 * self.quaternion_odo.x * self.quaternion_odo.y,0 ])
                od = np.array([self.position_odo.x,self.position_odo.y, self.position_odo.z])

            pd = np.dot((np.eye(2) - np.tensordot(vect_v, vect_v,axes = 0), (od- op0)))
            dist = np.sign(np.dot(pd,forward)) * np.linalg.norm(pd)

            theta = np.arctan2(np.cross(pd/np.linalg.norm(pd), forward)[2], np.dot(pd/np.linalg.norm(pd), forward))



            #Remise en place
            msg_linxrm = Float32()
            msg_rtrm= Float32()

            msg_linxrm.data = -0.2 * np.sign(dist)
            msg_rtrm.data = -0.5 * np.sign(theta)

            self.pub_rotate.publish(msg_rtrm)
            self.pub_linx.publish(msg_linxrm)

            temps = time()
        


        self.pub_hover.publish(Empty())

        #Slide
        self.pub_liny.publish(msg_liny)  
            




        


        

if __name__ == '__main__':

    rospy.init_node('slide')
    slide=Slide()
    rospy.spin()
