#!/usr/bin/env python

from std_msgs.msg import String
from drone_behavior import drone_behavior_class as db
import rospy
from time import time
from drone_behavior.msg import BehaviorStatus
from drone_behavior import drone_behavior_class
from multiprocessing import Lock

class Scheduler(object):
    def __init__(self):

        self.behaviors=['Hover','Land','Left','Right','TurnLeft','TurnRight','Up','Down','Uturn','TakeOff','Forward',
        'Backward','MoveForwardVp','AlignCorridor','CenterCorridor', 'Slide']

        self.commands={
            'Forward':[(0,'Hover'),(0.3,'Forward')],
            'Backward':[(0,'Hover'),(0.3,'Backward')],
            'Right':[(0,'Hover'),(0.3,'Right')],
            'Left':[(0,'Hover'),(0.3,'Left')],
            'TurnLeft':[(0,'Hover'),(0.3,'TurnLeft')],
            'TurnRight':[(0,'Hover'),(0.3,'TurnRight')],
            'Up':[(0,'Hover'),(0.3,'Up')],
            'Down':[(0,'Hover'),(0.3,'Down')],
            'Hover':[(0,'Hover')],
            'Land':[(0,'Hover'),(0.3,'Land')],
            'TakeOff':[(0,'TakeOff'),(2,'Hover')], 
            'Uturn': [(0,'Hover'),(0.3,'Uturn')],
            'Slide': [(0,'Hover'),(0.3,'Slide')],
            'GoAhead':[(0,'MoveForwardVp'),(0,'AlignCorridor'),(0,'CenterCorridor')],
            'AlignCorridor':[(0,'AlignCorridor')] ,
            'CenterCorridor':[(0,'CenterCorridor'),(0,'AlignCorridor')]
            
        }

        #définir le subscriber aux commandes
        self.command=rospy.Subscriber('command',String,self.on_command,queue_size=1)
        #définir un publisher de behaviors à executer 
        self.pub_behavior=rospy.Publisher('behavior',BehaviorStatus,queue_size=10)
        #self.sub_aligncorridor=rospy.Subscriber('vanishing_point',Float32MultiArray,self.on_status_on,queue_size=1)

        self.agenda=[]
        temps=time()
        
            
    def on_command(self,msg):
        temps=time()
        self.deactivate()
        TodoList=self.commands[msg.data]
        for task in TodoList:
            self.agenda.append([task[0]+temps,task[1]])

    def deactivate(self):
        for behavior in self.behaviors:
            msg=BehaviorStatus(behavior,False) 
            self.pub_behavior.publish(msg)

    def loop(self):
        while not rospy.is_shutdown():
            current_time=time()

            top_date = None
            if len(self.agenda)!=0:
                top_date,top_behavior = self.agenda[0]

    
            while top_date is not None and top_date < current_time:
                self.pub_behavior.publish(BehaviorStatus(top_behavior,True))
                self.agenda=self.agenda[1:]
                top_date = None
                if len(self.agenda)!=0:
                    top_date,top_behavior = self.agenda[0]    


            rospy.sleep(0.1)
    

if __name__ == '__main__':
    rospy.init_node('command')
    Scheduler().loop()

    rospy.spin()         




        
    


