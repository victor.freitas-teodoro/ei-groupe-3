#!/usr/bin/env python


from turtle import backward
from std_msgs.msg import Float32
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class Forward(db.Behavior):
    def __init__(self):
        super().__init__("Forward")
        
        self.pub_Forward=rospy.Publisher("/linx",Float32,queue_size=1)

    def on_status_on(self):
        msg=Float32()
        msg.data=db.linear_speed
        self.pub_Forward.publish(msg)
        

if __name__ == '__main__':

    rospy.init_node('Forward')

    forward=Forward()

    rospy.spin()