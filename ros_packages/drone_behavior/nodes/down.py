#!/usr/bin/env python



from turtle import down
from std_msgs.msg import Float32
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class Down(db.Behavior):
    def __init__(self):
        super().__init__("Down")
        
        self.pub_Down=rospy.Publisher("/linz",Float32,queue_size=1)

    def on_status_on(self):
        msg=Float32()
        msg.data=(-1)*db.vertical_speed
        self.pub_Down.publish(msg)
        

if __name__ == '__main__':

    rospy.init_node('Down')

    down=Down()

    rospy.spin()