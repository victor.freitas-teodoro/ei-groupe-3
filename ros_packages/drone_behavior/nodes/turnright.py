#!/usr/bin/env python


from std_msgs.msg import Float32
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class TurnRight(db.Behavior):
    def __init__(self):
        super().__init__("TurnRight")
        
        self.pub_TurnRight=rospy.Publisher("/angz",Float32,queue_size=1)

    def on_status_on(self):
        msg=Float32()
        msg.data=(-1)*db.angular_speed
        self.pub_TurnRight.publish(msg)
        

if __name__ == '__main__':

    rospy.init_node('TurnRight')

    turnright=TurnRight()

    rospy.spin()