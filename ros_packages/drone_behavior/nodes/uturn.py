#!/usr/bin/env python

from std_msgs.msg import Float32
from std_msgs.msg import Empty 
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time
import numpy as np


class Uturn(db.Behavior):
    def __init__(self):
        super().__init__("Uturn")
        
        self.pub_rotate=rospy.Publisher("/angz",Float32,queue_size=1)
        self.pub_hover=rospy.Publisher("/hover",Empty,queue_size=1)

    def on_status_on(self):
        msg_rotate=Float32()
        msg_rotate.data=db.angular_speed

        self.pub_rotate.publish(msg_rotate)
        
        rospy.sleep(3.6)
        
        self.pub_hover.publish(Empty())

        rospy.sleep(2)

        self.set_status()
        

if __name__ == '__main__':

    rospy.init_node('uturn')
    uturn=Uturn()
    rospy.spin()