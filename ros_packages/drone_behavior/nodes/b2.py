#!/usr/bin/env python

import rospy 
from drone_behavior import drone_behavior_class as db

if __name__ == '__main__':
    rospy.init_node('Test2')

    behavior2=db.Behavior("Test2")

    rospy.spin()
