#!/usr/bin/env python

from cmath import inf
import rospy
from drone_behavior import drone_behavior_class as db
from std_msgs.msg import Float32, Float32MultiArray
from multiprocessing import Lock


class AlignCorridor(db.Behavior):

    def __init__(self) :
        super().__init__("AlignCorridor")
        self.flag = False
        self.vp = Float32MultiArray()
        self.mutex=Lock()
        self.pub_aligncorridor=rospy.Publisher('/angz',Float32,queue_size=1)
        self.sub_aligncorridor=rospy.Subscriber('vanishing_point',Float32MultiArray,self.on_vanish,queue_size=1)

    def on_vanish(self, msg):
        self.vp = msg.data

    def on_status_off(self):
        self.flag = False

    def on_status_on(self):
        rospy.loginfo("On status on for align corridor !!!!")
        self.flag = True
    
    def pub(self):
        mymsg=Float32()
        mid = 430
        margin = 10
        with self.mutex:
            vp_now = self.vp


        x_vp,y_vp=vp_now[0],vp_now[1]

        if x_vp!=inf and x_vp>mid + margin:
            mymsg.data=(-1)* 0.05
            

        elif x_vp!=inf and x_vp<mid - margin:
            mymsg.data=0.05

        else:
            mymsg.data = 0

        self.pub_aligncorridor.publish(mymsg)

    def loop(self):
        while not rospy.is_shutdown():
            if self.flag:
                self.pub()

if __name__ == '__main__':

    rospy.init_node('AlignCorridor')

    aligncorridor=AlignCorridor()
    aligncorridor.loop()

    rospy.spin()  