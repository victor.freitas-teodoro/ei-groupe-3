from cmath import inf
import rospy
from drone_behavior import drone_behavior_class as db
from std_msgs.msg import Float32,Float32MultiArray
from multiprocessing import Lock

class MoveForwardVp(db.Behavior):

    def __init__(self) :
        super().__init__("MoveForwardVp")
        self.vp = Float32MultiArray()
        self.flag=False
        self.mutex=Lock()
        self.pub_moveforwardvp=rospy.Publisher('/linx',Float32,queue_size=1)
        self.sub_moveforwardvp=rospy.Subscriber('vanishing_point',Float32MultiArray,self.on_vanish,queue_size=1)
    
    def on_status_off(self):
        self.flag = False

    def on_status_on(self):
        self.flag = True
        
    def on_vanish(self, msg):
        self.vp = msg.data

    def pub(self):

        mymsg=Float32()

        with self.mutex:
            vp_now = self.vp

        x_vp,y_vp=vp_now[0],vp_now[1]

        if x_vp!=inf and y_vp!=inf:
            mymsg.data=db.linear_speed

        else:
            mymsg.data=0
        self.pub_moveforwardvp.publish(mymsg)

    def loop(self):
        while not rospy.is_shutdown():
            if self.flag:
                self.pub()

if __name__ == '__main__':

    rospy.init_node('Forward')

    moveforwardvp=MoveForwardVp()

    moveforwardvp.loop()  