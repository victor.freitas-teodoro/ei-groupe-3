#!/usr/bin/env python

from turtle import left, right
from std_msgs.msg import Float32
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class Right(db.Behavior):
    def __init__(self):
        super().__init__("Right")
        
        self.pub_Right=rospy.Publisher("/liny",Float32,queue_size=1)

    def on_status_on(self):
        msg=Float32()
        msg.data= -1 * db.linear_speed
        self.pub_Right.publish(msg)
        

if __name__ == '__main__':

    rospy.init_node('Right')

    right=Right()

    rospy.spin()