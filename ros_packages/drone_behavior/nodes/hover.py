#!/usr/bin/env python

from std_msgs.msg import Empty
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class Hover(db.Behavior):
    def __init__(self):
        super().__init__("Hover")
        
        self.pub_Hover=rospy.Publisher("/hover",Empty,queue_size=1)

    def on_status_on(self):
        self.pub_Hover.publish(Empty())
        rospy.sleep(2)
        self.set_status()

if __name__ == '__main__':

    rospy.init_node('Hover')

    hover=Hover()

    rospy.spin()
