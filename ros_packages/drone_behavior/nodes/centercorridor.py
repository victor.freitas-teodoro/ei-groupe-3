#!/usr/bin/env python


import rospy
from drone_behavior import drone_behavior_class as db
from std_msgs.msg import Float32,Float32MultiArray
from multiprocessing import Lock

class CenterCorridor(db.Behavior):

    def __init__(self) :
        super().__init__("CenterCorridor")
        self.flag = False
        self.vp = Float32MultiArray()
        self.mutex=Lock()
        self.pub_centercorridor=rospy.Publisher('/liny',Float32,queue_size=1)
        self.sub_centercorridor=rospy.Subscriber('vanishing_point',Float32MultiArray,self.on_vanish,queue_size=1)
    
    def on_vanish(self, msg):
        self.vp = msg.data

    def on_status_off(self):
        self.flag = False

    def on_status_on(self):
        self.flag = True

    def pub(self):
        mymsg=Float32()
        scale = 0.1
        with self.mutex:
            vp_now = self.vp
        x_vp,y_vp,position_drone=vp_now[0],vp_now[1],vp_now[2]

        if x_vp!=0 and position_drone<0 :
            mymsg.data=db.linear_speed*scale
        
        elif x_vp!=0 and position_drone>0 :
            mymsg.data=(-1)*db.linear_speed*scale

        else:
            mymsg.data=0
            
        self.pub_centercorridor.publish(mymsg)
            
    def loop(self):
        while not rospy.is_shutdown():
            if self.flag:
                self.pub()

if __name__ == '__main__':

    rospy.init_node('CenterCorridor')

    centercorridor=CenterCorridor()

    centercorridor.loop()  