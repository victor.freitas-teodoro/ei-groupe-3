#!/usr/bin/env python


from std_msgs.msg import Float32
from drone_behavior import drone_behavior_class as db
import rospy
from drone_behavior.msg import BehaviorStatus
from time import time


class Up(db.Behavior):
    def __init__(self):
        super().__init__("Up")
        
        self.pub_Up=rospy.Publisher("/linz",Float32,queue_size=1)

    def on_status_on(self):
        msg=Float32()
        msg.data=db.vertical_speed
        self.pub_Up.publish(msg)
        

if __name__ == '__main__':

    rospy.init_node('Up')

    up=Up()

    rospy.spin()