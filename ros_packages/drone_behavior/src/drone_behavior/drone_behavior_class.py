
from drone_behavior.msg import BehaviorStatus
import rospy
import numpy as np
from multiprocessing import Lock
linear_speed=0.2
vertical_speed=0.2
angular_speed=0.5
class Behavior(object):

    def __init__(self,name):
        self.name=name
        self.status=False
        self.status_mutex=Lock()

    #defining the message received by the node
        self.BehaviorMessage=rospy.Subscriber("/behavior",BehaviorStatus,self.UpdateStatus,queue_size=1)
   
    #publishers
        self.pub_status=rospy.Publisher("behaviors_status",BehaviorStatus,queue_size=1)
   

    def get_status(self):
        with self.status_mutex:
            copy=self.status
        return(copy)

    def UpdateStatus(self,msg):
        if msg.name==self.name: 
            with self.status_mutex:
                self.status=msg.status
            if msg.status==True:
                self.on_status_on()
            else:
                self.on_status_off()
        elif msg.name=='ping':
            rspns=BehaviorStatus(self.name,self.get_status())
            self.pub_status.publish(rspns)

    def set_status(self):
        self.status=False
    
    def on_status_on(self) :
        pass
    
    def on_status_off(self) :
        pass
    


        


