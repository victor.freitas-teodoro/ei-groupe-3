# Importing cv
import cv2 as cv
import numpy as np


# Reading an image in default mode
img = cv.imread('src/image.jpeg')
img_2 = cv.imread('src/mario.jpeg.jpeg')

# Gray scaling
# Gray scaling
img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
img_gray_2 = cv.cvtColor(img_2, cv.COLOR_BGR2GRAY)


matrix = np.vstack((img_gray[190, 0:7],img_gray[190, 1:8]))

print(matrix)
sub = matrix - img_gray_2[190, 0:7] 
sub = sub**2
print(sub)
soma = np.sum(sub, axis=1)
print(soma)
print(np.argmin(soma))
# Creates an image filled with zero
# intensities with the same dimensions 
# as the frame
mask = np.zeros_like(img_gray)
  
# Sets image saturation to maximum
mask[..., 1] = 255


def draw_function(img, hmin, ymin, hmax, ymax): 
    color     = (255, 0, 0)
    thickness = 3 
    image_fin = cv.line(img, (hmin, ymin), (hmax, ymax), color, thickness)
    return image_fin

image_f = draw_function(img_gray, 0, 2000, 3000, 2000)


# showing image
# Naming a window
cv.namedWindow("Resized_Window", cv.WINDOW_NORMAL)
  
# Using resizeWindow()
cv.resizeWindow("Resized_Window", 300, 1500)
  
# Displaying the image
cv.imshow("Resized_Window", image_f)
cv.waitKey(0)