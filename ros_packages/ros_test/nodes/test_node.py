#! /usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import rospy
from sensor_msgs.msg import CompressedImage

# import your_visual_processing_library

class Test:
    
    def __init__(self):
        self.sub = rospy.Subscriber("/image_in/compressed",  CompressedImage, self.on_image,  queue_size = 1, buff_size=2**22)
        
    def on_image(self, msg):
        compressed_in = np.frombuffer(msg.data, np.uint8)
        frame         = cv2.imdecode(compressed_in, cv2.IMREAD_COLOR)

        # if time is required in your cumputation, consider the current time
        # as the one recorded in the message timestamps. This enables a coherent
        # time sequence when the images come from a ros bag, since ros bag execution
        # can be paused, run step by step, ...
        now = msg.header.stamp
        # your_visual_processing_library.your_algo(frame, now)
        print(frame, now)


rospy.init_node('test')
test = Test()
rospy.spin()
