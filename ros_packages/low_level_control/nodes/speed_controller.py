#! /usr/bin/env python

import rospy
import numpy as np
import datetime

# Type de message 
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from std_msgs.msg import Empty
from nav_msgs.msg import Odometry

#Protection 
from multiprocessing import Lock

class SpeedController(object):
    
    def __init__(self):
        #Paramètre (à modifer avec vrai paramètres)
        self.max_speedlin_z = 2
        self.max_speedang_z = 10

        #Mutex
        self.data_mutex = Lock()

        #Odometry
        self.curent_speed = Twist()

        # Gain
        self.gains = {
                "linear_x":   {'Kp': 0.35, 'Kd': 0.0, 'Ki': (0.35 * 0.54) /2},
                "linear_y":  {'Kp': 0.35, 'Kd': 0.0, 'Ki': (0.35 * 0.54) /2}
                } 

        #PID
        self.pid_roll= PID(self.gains["linear_x"])
        self.pid_pitch= PID(self.gains["linear_y"])

        #Vitesse
        self.cmd= Twist()

        #hover mode 
        self.hover = False

        # Publishers
        self.cmd_vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size = 1)

        # Suscriber
        self.linear_x_sub = rospy.Subscriber("/linx",  Float32 ,self.on_linx  , queue_size = 1)
        self.linear_y_sub = rospy.Subscriber("/liny",  Float32 ,self.on_liny  , queue_size = 1)
        self.linear_z_sub = rospy.Subscriber("/linz",  Float32 ,self.on_linz  , queue_size = 1)
        self.angular_z_sub = rospy.Subscriber("/angz",  Float32 ,self.on_angz  , queue_size = 1)

        self.hover_sub = rospy.Subscriber('/hover', Empty, self.on_hover, queue_size =1)
        self.odom_sub = rospy.Subscriber('/odom', Odometry, self.on_odom, queue_size =1)
    
    # Mode survol
    def on_hover(self,msg):
        with self.data_mutex :
            self.cmd = Twist()
            self.pid_roll.reset()
            self.pid_pitch.reset()
            self.hover = True

    # Récupération Odométrie
    def on_odom(self,msg):
        with self.data_mutex :
            self.curent_speed = msg.twist.twist

    # Récupération des vitesses
    def on_linx(self,msg):
        with self.data_mutex :
            self.cmd.linear.x = msg.data
            self.hover = False 
    
        
    def on_liny(self,msg):
        with self.data_mutex :
            self.cmd.linear.y = msg.data
            self.hover = False 
   
        

    def on_linz(self,msg):
        with self.data_mutex :
            self.cmd.linear.z = msg.data
            self.hover = False 
            
        
    
    def on_angz(self,msg):
        with self.data_mutex :
            self.cmd.angular.z = msg.data
            self.hover = False 
            

        
    def loop(self):
        while not rospy.is_shutdown():
            with self.data_mutex :
                if self.hover:
                    self.cmd_vel_pub.publish(Twist())
                else : 
                    #Initialisation de la pub
                    cmd_public = Twist()
                    # vitesse z
                    cmd_public.linear.z = self.cmd.linear.z 
                    cmd_public.angular.z = self.cmd.angular.z 

                    #roll / pitch

                    #temps
                    t_ros = rospy.Time.now()
                    t_datetime = datetime.datetime.utcfromtimestamp(t_ros.to_sec()) 

                    #Mise à jour erreur 
                    self.pid_roll.update(t_datetime, self.cmd.linear.x - self.curent_speed.linear.x ) 
                    self.pid_pitch.update(t_datetime, self.cmd.linear.y - self.curent_speed.linear.y) 

                    # Calcule commande
                    cmd_public.linear.x = self.pid_roll.command
                    cmd_public.linear.y = self.pid_pitch.command

                    self.cmd_vel_pub.publish(cmd_public)

            rospy.sleep(0.1)





class PID(object):
    def __init__(self, gains):

        self.gains = gains.copy()
        self.errors = {'error' : 0 , 'd_error' : 0, 'i_error': 0 , 'time' : None}
        
    
    def reset(self):
        self.errors = {'error' : 0 , 'd_error' : 0, 'i_error': 0 , 'time' : None}

    def update(self,
               time: datetime.datetime,
               error: float):

        prev_error = self.errors['error']
        prev_time = self.errors['time']
        if prev_time is None:
            self.errors['error'], self.errors['time'] = error, time
            return

        self.errors['error'] = error
        self.errors['time'] = time
        dt = (time - prev_time).total_seconds()
        self.errors['i_error'] += dt * (prev_error + error)/2.0
        self.errors['d_error'] = (error - prev_error)/dt

    @property
    def command(self):
        return self.gains['Kp'] * self.errors['error'] + \
                self.gains['Kd'] * self.errors['d_error'] + \
                self.gains['Ki'] * self.errors['i_error']
    






        
                







if __name__ == '__main__':

    rospy.init_node('speed_controller')
    speed_controller = SpeedController()
    speed_controller.loop()
    rospy.spin()

