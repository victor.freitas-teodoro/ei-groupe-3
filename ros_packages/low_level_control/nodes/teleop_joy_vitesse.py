#!/usr/bin/env python

import rospy

# Type de message 
from sensor_msgs.msg   import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty
from std_msgs.msg import Float32



class TeleopeJoy(object):

    def __init__(self) :

        #Param
        self.linear_speedmax = 1

        # Etat initial (Etat "au sol")
        self.land = True

        # Publishers
        self.linear_x_pub = rospy.Publisher("/linx", Float32, queue_size = 1)
        self.linear_y_pub = rospy.Publisher("/liny", Float32, queue_size = 1)
        self.linear_z_pub = rospy.Publisher("/linz", Float32, queue_size = 1)
        self.angular_z_pub = rospy.Publisher("/angz", Float32, queue_size = 1)
        self.hover_pub = rospy.Publisher('/hover', Empty, queue_size =1)

        self.land_pub = rospy.Publisher("/land", Empty, queue_size = 1)
        self.takeof_pub = rospy.Publisher("/takeoff", Empty, queue_size = 1)

        # Suscriber
        self.joy_sub = rospy.Subscriber("/joy",  Joy , self.on_joy , queue_size = 1)
        
    def on_joy(self, msg):
        message = Empty()


        # Décolage
        if msg.axes[5] == -1 and self.land:
            self.takeof_pub.publish(message)
            self.land = False

        # Commande de vol
        elif msg.axes[5] == -1 and (not self.land):
            # Initilisation 

            #Creation message

            self.linear_x_pub.publish(msg.axes[1]*self.linear_speedmax)
            self.linear_y_pub.publish(msg.axes[0]*self.linear_speedmax)
            self.linear_z_pub.publish(msg.axes[4]*self.linear_speedmax)
            self.angular_z_pub.publish(msg.axes[3])

            if msg.buttons[0] == 1.0:
                self.hover_pub.publish(message)

        #Attérisage
        else : 
            #Arret 
            self.linear_x_pub.publish(0)
            self.linear_y_pub.publish(0)
            self.linear_z_pub.publish(0)
            self.angular_z_pub.publish(0)
            self.land_pub.publish(message)
            self.land = True




        
if __name__ == '__main__':  

    rospy.init_node('teleop_joy')
    teleop_joy = TeleopeJoy()    
    rospy.spin() 