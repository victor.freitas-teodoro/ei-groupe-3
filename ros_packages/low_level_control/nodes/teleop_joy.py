#!/usr/bin/env python

import rospy

# Type de message 
from sensor_msgs.msg   import Joy
from geometry_msgs.msg import Twist
from std_msgs.msg import Empty



class TeleopeJoy(object):

    def __init__(self) :

        # Etat initial (Etat "au sol")
        self.land = True

        # Publishers
        self.cmd_vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size = 1)
        self.land_pub = rospy.Publisher("/land", Empty, queue_size = 1)
        self.takeof_pub = rospy.Publisher("/takeof", Empty, queue_size = 1)

        # Suscriber
        self.joy_sub = rospy.Subscriber("/joy",  Joy , self.on_joy , queue_size = 1)
        
    def on_joy(self, msg):
        message = Empty()


        # Décolage
        if msg.axes[5] == -1 and self.land:
            self.takeof_pub.publish(message)
            self.land = False

        # Commande de vol
        elif msg.axes[5] == -1 and (not self.land):
            # Initilisation 
            cmd_vel = Twist()

            #Creation message
            cmd_vel.linear.x = msg.axes[1]
            cmd_vel.linear.y  = msg.axes[0]
            cmd_vel.linear.z = msg.axes[4]
            cmd_vel.angular.z = msg.axes[3]

            self.cmd_vel_pub.publish(cmd_vel)

        #Attérisage
        else : 
            #Arret 
            cmd_arr = Twist()
            self.cmd_vel_pub.publish(cmd_arr)
            self.land_pub.publish(message)
            self.land = True




        
if __name__ == '__main__':  

    rospy.init_node('teleop_joy')
    teleop_joy = TeleopeJoy()    
    rospy.spin() 