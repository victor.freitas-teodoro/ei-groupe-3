#!/usr/bin/env python

import rospy
from copy import deepcopy

# import the message type you need (here are examples)
from std_msgs.msg      import Float32, Empty, Float32MultiArray
from nav_msgs.msg      import Odometry
from sensor_msgs.msg   import CompressedImage
from geometry_msgs.msg import Twist

# If you need to protect a variable by a mutex...
from multiprocessing import Lock

# Then, as usual
import numpy as np

# And for image processing
import cv2

# Import our package
from computerVision import detect, draw


class VisualProcessing:

    def __init__(self) : # We are in thread #1 here.
        # let us declare attributes that can be retrieved easily in the different methods.

        # getting params for depth calculations
        self.window_size = get_param('~ls_window_size', 100)
        self.range_s = get_param('~range_shifts', 20)
        self.filter_window = get_param('~filter_window_size', 50)
        self.n_line = get_param('~n', 400) 

        # getting params for vanishing point calculations
        self.confidence = get_param('~sci_confidence', .3)
        self.lower = get_param('~vlines_cutoff', .2)
        self.upper = get_param('~hlines_cutoff', .99)
        self.min_length = get_param('~min_line_length', 40)
        self.max_vp = get_param('~max_vp_lines_distance', 50)

        
        self.prev = None

        self.depth_img = None
        self.image = None         # this will be thread protected...
        self.vp = None
        self.odom = None

        self.data_mutex = Lock() # ... thanks to this mutex.

        # Let us define the publishers
        self.pub_image_depth = rospy.Publisher("/bebop/image_processed_depth/compressed", CompressedImage, queue_size = 1)
        self.pub_image = rospy.Publisher("/bebop/image_processed/compressed", CompressedImage, queue_size = 1)
        self.pub_vp = rospy.Publisher("vanishing_point", Float32MultiArray, queue_size=5)
        # Let us define some subscribers. Each declaration created a thread.
        self.sub_image = rospy.Subscriber("/bebop/image_raw/compressed",  CompressedImage,
                                          self.on_image,   # This is thread #2
                                          queue_size = 1, buff_size=2**22) # This buff_size tricks prevents from delay in image stream.
        self.sub_slide = rospy.Subscriber("/bebop/odom", Odometry, self.on_odom, queue_size = 1, buff_size=2**22)

    def on_odom(self, msg):
        if self.pub_image_depth.get_num_connections() > 0:
            self.odom = msg

    # This called within thread #2 when an image message is recieved.
    def on_image(self, msg):
        # From ros message to cv image
        compressed_in = np.frombuffer(msg.data, np.uint8)
        frame         = cv2.imdecode(compressed_in, cv2.IMREAD_COLOR)

        # Then we can play with opencv
        gray          = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        width         = gray.shape[1]
        height        = gray.shape[0]

        # Let us publish the processed image
        if self.pub_image_depth.get_num_connections() > 0:
            self._publish_image_depth(gray)

        if self.pub_image.get_num_connections() > 0:
            self._publish_image(frame, gray)
        
        if self.pub_vp.get_num_connections() > 0:
            self._publish_vp(gray)

        self.prev = gray      

    def _publish_image(self, frame, gray):
        # Let us lock data in order to compute if from the frame
        with self.data_mutex:
            lines = detect.lines(gray)
            lines = detect.best_lines(lines, self.min_length, self.lower, self.upper)
            vp = detect.vanishing_point(lines, self.confidence)
            supporting_lines = detect.vp_lines(lines, vp)
            self.image = draw.vanishing_point(frame, vp, supporting_lines)
        msg              = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format       = "jpeg"
        msg.data         = np.array(cv2.imencode('.jpg', self.get_data()[0])[1]).tobytes()
        self.pub_image.publish(msg)

    def _publish_image_depth(self, gray):
        odom = self.get_data()[3]

        # finding the direction of movement
        if odom is not None:
            yvel = odom.twist.twist.linear.y
            dir = 1 if yvel > 0 else -1
        else:
            dir = -1
        
        # updating data
        with self.data_mutex:
            self.depth_img = draw.depth(self.prev, gray, dir, self.n_line, (0,255,0), self.window_size, self.range_s, self.filter_window)
        msg              = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format       = "jpeg"
        msg.data         = np.array(cv2.imencode('.jpg', self.get_data()[2])[1]).tobytes()
        self.pub_image_depth.publish(msg)

    def _publish_vp(self, gray):
        with self.data_mutex:
            lines = detect.lines(gray)
            lines = detect.best_lines(lines, self.min_length, self.lower, self.upper)
            vp = detect.vanishing_point(lines, self.confidence)
            dist = detect.wall_dist(lines,vp)
            self.vp = np.array([vp[0], vp[1], dist])
        msg      = Float32MultiArray()
        msg.data = self.get_data()[1]
        self.pub_vp.publish(msg)


    def get_data(self) :
        with self.data_mutex :
            image_copy = np.copy(self.image)
            array_copy = np.copy(self.vp)
            depth_img_copy = np.copy(self.depth_img)
            odom_copy = deepcopy(self.odom)
        return image_copy, array_copy, depth_img_copy, odom_copy

    # This is in the main thread #1
    def loop(self):
        while not rospy.is_shutdown():
            ratio_msg = Float32()
            ratio_msg.data = self.get_data() # we get the data, being protected from thread #2 writings.
            rospy.sleep(0.1) # we sleep for 100ms


def get_param(name, default=None):
    """Verbose parameter getter.

    Checks for parameter in parameter server using default if needed.

    Args:
        name: the name of the parameter.
        default: an optional default value.

    Raises:
        KeyError: if neither parameter nor default value are there.
    """
    if rospy.has_param(name):
        val = rospy.get_param(name)
        rospy.loginfo('Found parameter %r: %r', name, val)
        return val
    rospy.logwarn('Parameter %r not found, using default value %r.',
                    name, default)
    return default

def main():
    """Create ROS node and instantiate class."""
    try:
        rospy.init_node('ComputerVision')
        node = VisualProcessing()
        node.loop()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    print("Initializing node")
    main()
